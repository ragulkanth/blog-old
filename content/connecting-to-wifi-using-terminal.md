Title: Connecting to Wifi using Terminal
Date: 2017-08-13
Category: Post, Terminal, CLI

In this article we are gonna see how to turn on wifi and connect to a network wireless using terminal or by command line interface (CLI).

**Step 1:**
First we have to find out the interface name of your wifi hardware. In GNU/Linux each and every networking hardware have an unique name (Tips: Interface name which begins with 'e' are ethernet hardware and which begins with 'w' are wireless hardware why because we may have one or more network hardware). To list the network hardwares openup terminal and type the following.

``` shell
ip addr
```

You will get the output as following

``` shell
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: enp3s0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc mq state DOWN group default qlen 1000
    link/ether 40:16:7e:43:84:9f brd ff:ff:ff:ff:ff:ff
3: wlp2s0f0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP group default qlen 1000
    link/ether 54:35:30:3a:8b:db brd ff:ff:ff:ff:ff:ff
```

So here I have two network hardware connected to my computer but it listed three. The first one is the system loopback and other two are the physical network hardware in which the third one is the wireless hardware. Here the name of my wifi hardware is wlp2s0f0. (Note: wlp2s0f0 - this may vary depends on the GNU/Linux distro that you are using)

**Step 2:**
In this step we are actually going to turn on the wifi hardware. Lets do it.

``` shell 
sudo ip link set wlp2s0f0 up
```

So the wifi hardware is turned on. Now whats next?. Lets list the wifi network signals available. For hidden wireless networks skip this part and move on to next. 

``` shell
iwlist wlp2s0f0 scanning | grep ESSID
```
Output:
``` shell
	ESSID:"hello1"
	ESSID:"hello2"
```

**Step 3:**
Now we have to create a configuration file, in order to connect to the wireless network. Lets assume that we are going to connect to the wireless network 'hello1' whose password is 'hellopelican'

``` shell
wpa_passphrase hello1 hellopelican > hello1.conf
```

The file will be written with the following

``` shell
network={
	ssid="hello1"
	#psk="hellopelican"
	psk=73bababe080c72a74af8129a9cf7d5719e46ecac9e5026e2324a5f40ff425c9d
}
```

We are done with the configuration. Next we have to connect to the network.

``` shell 
sudo wpa_supplicant -i wlp2s0f0 -c hello1.conf -B
```

This will show you the following.

``` shell
Successfully initialized wpa_supplicant
```

Still it is not that you are connected. For verifying it do the following.

``` shell
ip addr
```

If it shows the following output then you are connected if not you went wrong.

``` shell
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: enp3s0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc mq state DOWN group default qlen 1000
    link/ether 40:16:7e:43:84:9f brd ff:ff:ff:ff:ff:ff
3: wlp2s0f0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP group default qlen 1000
    link/ether 54:35:30:3a:8b:db brd ff:ff:ff:ff:ff:ff
    inet 192.168.43.225/24 brd 192.168.43.255 scope global wlp2s0f0
       valid_lft forever preferred_lft forever
    inet6 fe80::e844:a9b8:2c01:692c/64 scope link 
       valid_lft forever preferred_lft forever
```

**Helps and Tips**
If you find any one of the command not found do the following.

``` shell
# Debian and Ubuntu based distro
sudo apt-get install iw wpa_supplicant

#Fedora 
sudo dnf install iw wpa_supplicant

#Arch based distro
sudo pacman -Sy iw wpa_supplicant
```

If any help needed regarding this article please be free to contact me at ragul274@gmail.com
