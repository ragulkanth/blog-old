Title: Gitpages/Blog using Python and Pelican
Date: 2017-03-08
Category: Post, Python, Pelican, Markdown, Blogging

In this article you are going to learn how to cook Gitpages/Blog using Python, Pelican, Markdown, Typogrify and Git. This is very simple so anyone can do it and there is no need of any prior knowledge of Python. If you are a python develeoper, it will be pretty much easy to learn this. For this article I used GNU/Linux operating system Fedora distribution and Gitlab. Now what lets go.

**Ingredients for cooking Gitpages/Blog**

* Computer installed with GNU/Linux operating system
* Python libraries [Pelican][pelican-doc], Markdown and Typogrify
* Gitlab Account

Most of the GNU/Linux distributions are comming with Python and GIT preinstalled. Now what you have to do is to [create an account in Gitlab][gitlab]. You are ready to cook Gitpages/Blog.

**Now Let's start cooking...**

**Step 1:**
First you have to verify whether the Python and GIT are installed. Open terminal by clicking it in your menu or press **Ctrl+Alt+T**(If you are using Debian based distro). Type the following in the terminal.

``` shell
python --version
```

``` shell
git --version
```
You will get the following output

``` bash
[ragulkanth@localhost ~]$ python --version
Python 2.7.11
[ragulkanth@localhost ~]$ git --version
git version 2.5.5
[ragulkanth@localhost ~]$ 
```

If it is not showing the above output just install them by means of searching the web with your operating system specified.

**Step 2:**
Now you have to install some of the python libraries that is needed for our cooking. Lets do it.

``` shell
sudo pip install pelican markdown typogrify
```

**Step 3:**
In the above two steps we have prepared our vessels that is needed for cooking. In this step we are actually going to start our cooking and don't forget, our recepie is Gitpages/Blog.

Now you have to prepare folder that is specifically for holding your blog files and get into it. For that open your terminal and type the following.

``` shell
mkdir blog
cd blog
```

Now you are inside your blog folder.

**Step 4:**
In this step you are going to create the files that is needed for your Gitpages/Blog. Do the following in your terminal.

``` shell
pelican-quickstart
```

You should do the following with some changes

![screenshot-pelican-quickstart](https://ragulkanth.gitlab.io/blog/images/screenshot-pelican-quickstart.jpg)

For Title, Author name, Language, URL-Prefix and Time Zone specify your details. Now see your blog folder roasted with the files. You are almost done.

**Step 5:**
In this step you have to learn Markdown from [here][markdown-tutorial]

**Step 6:**
Let me assume that you have learned how to cook with markdown from the previous step. So in this step we are almost done with the recepie by posting our fist post. Let's see how to do it. 

For this we are using markdown, hence the file extension for markdown is **.md**. These md files should be in the **content folder**. Now create the file **first-post.md** by doing the things that is mentioned below.

``` shell
gedit content/first-post.md
``` 

This will open gedit the text editor with the filename specified. Don't have gedit, just install it for your GNU/Linux distro :-). Now put the content below in it.

``` md
Title: First Post
Date: 2017-03-08
Category: Post

This is my first post :-).
```

Save it and quit the gedit.

_**Note:**_ If you want to add some images with your post, create a folder called images in the content folder and put your images in it. Still you have one more thing to do. Open the _pelicanconf.py_ using gedit and add the following line after the line ends with unicode_literals with a blankline added. 

``` python
STATIC_PATHS = ['images']
```

Now you have to roast the Gitpages/Blog by doing the following.

``` shell
[ragulkanth@localhost blog]$ pelican content
Done: Processed 1 article, 0 drafts, 0 pages and 0 hidden pages in 0.39 seconds.
[ragulkanth@localhost blog]$ 
```

To see your blog output do the following.

``` shell
[ragulkanth@localhost blog]$ cd output/
[ragulkanth@localhost output]$ python -m pelican.server
```

Now visit [localhost:8000](http://localhost:8000/) in your browser.

Hurrah! Our recepie is done. In the next step, you will learn how to serve it to everyone using [Gitlab][gitlab]

**Step 7:**
Come on lets serve :-).

For serving your recepie you have to prepare the dining table and the dishes. Here your dining table is Gitlab and your dish is Gitlab docker.

Your first most step is to prepare the dining table. For that do the following.

Before that you have to configure your git in your computer.

``` shell
[ragulkanth@localhost blog]$ git config --global user.name "ragulkanth"
[ragulkanth@localhost blog]$ git config --global user.email "ragul274@gmail.com"
```

_**Note:**_ Replace with your Name and E-mail.

Now login to your [Gitlab Account][Gitlab] and press New Project button. You will see something similar to the below image.

![screenshot-new-project](https://ragulkanth.gitlab.io/blog/images/screenshot-new-project.jpg)

Your project name should be _**blog**_ and your visiblity level should be _**Public**_ and any thing may be your description and press create button.

After this you have to create a configuration file called **.gitlab-ci.yml** in your blog folder with the following content.

``` yml
image: python:2.7-alpine

pages:
  stage: deploy
  script:
    - pip install pelican markdown typogrify
    - pelican -s publishconf.py
    - mkdir .public
    - cp -r output/* .public
    - mv .public public
  artifacts:
    paths:
    - public
```

Now you have to initiate the git in your blog folder and merge your gitlab account and add your files to your git repo by the following lines.

``` shell 
[ragulkanth@localhost blog]$ git init
[ragulkanth@localhost blog]$ git remote add origin git@gitlab.com:ragulkanth/blog.git
[ragulkanth@localhost blog]$ git add .
[ragulkanth@localhost blog]$ git commit -m "Commit message"
[ragulkanth@localhost blog]$ git push -u origin master
``` 
_**Note:**_ Change ragulkanth by your gitlab username and Commit message with the relavant message.

Now open your browser and type yourgitlabusername.gitlab.io/blog/. You can see that your Gitpage is hosted in [Gitlab][gitlab]. 

**Final Step:**
This step is specifically for the future updates in your blog. For that you have to follow the below steps.

Go to your content folder and create your _**next-post.md**_ file using gedit and add what's in your mind and save it. Then...

``` shell
[ragulkanth@localhost blog]$ pelican content
[ragulkanth@localhost blog]$ git add .
[ragulkanth@localhost blog]$ git commit -m "Next Commit message"
[ragulkanth@localhost blog]$ git push -u origin master
```

And Ensure the changes affected in your gitpages.

Hurrah! You have learned to cook the recepie Gitpages/Blog using python and Pelican and also learned to serve it through Gitlab pages.

Have any doubts regarding this recepie?. Just let me know via email: ragul274@gmail.com.

[markdown-tutorial]: http://www.markdowntutorial.com
[pelican-doc]: http://docs.getpelican.com/en/stable/
[gitlab]: https://gitlab.com
