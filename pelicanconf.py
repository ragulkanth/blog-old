#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

STATIC_PATHS = ['images']
AUTHOR = u'ragulkanth'
SITENAME = u'ragulkanth #DEV-IL'
SITEURL = ''
THEME = "Casper2Pelican"
DEFAULT_HEADER_IMAGE = "https://ragulkanth.gitlab.io/blog/theme/images/banner-bg.jpg"
ARCHIVE_HEADER_IMAGE = "https://ragulkanth.gitlab.io/blog/theme/images/banner-bg.jpg"
AUTHOR_PIC_URL = "https://ragulkanth.gitlab.io/blog/images/ragulkanth.jpg"
AUTHOR_BIO = "Developer and Illustrator"
AUTHOR_LOCATION = "Pondicherry"

PATH = 'content'

TIMEZONE = 'Asia/Kolkata'

DEFAULT_LANG = u'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Gitlab Repo', 'https://gitlab.com/ragulkanth'),
         ('Facebook', 'https://www.facebook.com/ragul.kanth.37'),
         ('Twitter', 'https://twitter.com/ragul274'),
         ('@ragulkanth:matrix.org', '#'),)

# Social widget
SOCIAL = (('Facebook', 'https://www.facebook.com/ragul.kanth.37'),
          ('Twitter', 'https://twitter.com/ragul274'),
          ('@ragulkanth:matrix.org', '#'))

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
